//
//  NavigationBarButtonBuilderFactory.swift
//  Ketab
//
//  Created by Vortex on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class NavigationBarButtonWithTextBuilder {
    
    private let button = UIButton(type: .custom)
    private let barButtonItem: UIBarButtonItem
    
    init() {
        barButtonItem = UIBarButtonItem(customView: button)
        button.layer.masksToBounds = true
        button.clipsToBounds = true
    }
    
    func title(_ title: String) -> Self {
        button.setTitle(title, for: .normal)
        return self
    }
    
    func font(_ font: UIFont) -> Self {
        button.titleLabel?.font = font
        return self
    }
    
    func titleColor(_ color: UIColor) -> Self {
        button.setTitleColor(color, for: .normal)
        return self
    }
    
    func backgroundColor(_ color: UIColor) -> Self {
        button.backgroundColor = color
        return self
    }
    
    func cornerRadius(_ radius: CGFloat) -> Self {
        button.layer.cornerRadius = radius
        return self
    }
    
    func action(target: Any, selector: Selector) -> Self {
        button.addTarget(target, action: selector, for: .touchUpInside)
        return self
    }
    
    func width(_ width: CGFloat) -> Self {
        barButtonItem.customView?.widthAnchor.constraint(equalToConstant: width).isActive = true
        return self
    }
    
    func height(_ height: CGFloat) -> Self {
        barButtonItem.customView?.heightAnchor.constraint(equalToConstant: height).isActive = true
        return self
    }
    
    public func build() -> UIBarButtonItem {
        return barButtonItem
    }
    
}

class NavigationBarButtonWithImageBuilder {
    
    private let button = UIButton(type: .system)
    private let barButtonItem: UIBarButtonItem
    
    init() {
        barButtonItem = UIBarButtonItem(customView: button)
        button.imageView?.contentMode = .scaleAspectFit
    }
    
    func image(_ image: UIImage?) -> Self {
        button.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
        return self
    }
    
    func action(target: Any, selector: Selector) -> Self {
        button.addTarget(target, action: selector, for: .touchUpInside)
        return self
    }
    
    func width(_ width: CGFloat) -> Self {
        barButtonItem.customView?.widthAnchor.constraint(equalToConstant: width).isActive = true
        return self
    }
    
    func height(_ height: CGFloat) -> Self {
        barButtonItem.customView?.heightAnchor.constraint(equalToConstant: height).isActive = true
        return self
    }
    
    public func build() -> UIBarButtonItem {
        return barButtonItem
    }
    
}

class NavigationBarButtonBuilderFactory {
    
    private init() {}
    
    class func buttonWithText() -> NavigationBarButtonWithTextBuilder {
        return NavigationBarButtonWithTextBuilder()
    }
    
    class func buttonWithImage() -> NavigationBarButtonWithImageBuilder {
        return NavigationBarButtonWithImageBuilder()
    }
    
}
