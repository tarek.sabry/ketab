//
//  FavoritesManager.swift
//  Ketab
//
//  Created by Vortex on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

protocol FavoritesManagerProtocol {
    var savedIndicies: [Int] { get }
    @discardableResult func toggleFavorite(at index: Int) -> Bool
}

class FavoritesManager: FavoritesManagerProtocol {
    
    var savedIndicies: [Int] {
        return UserDefaults.standard.array(forKey: "favorites") as? [Int] ?? [Int]()
    }
    
    @discardableResult func toggleFavorite(at index: Int) -> Bool {
        if isIndexSaved(index) {
            unfavorite(pageIndex: index)
            return false
        } else {
            favorite(pageIndex: index)
            return true
        }
    }
   
    private func isIndexSaved(_ index: Int) -> Bool {
        return savedIndicies.filter { $0 == index }.count > 0
    }
    
    private func favorite(pageIndex: Int) {
        if var array = UserDefaults.standard.array(forKey: "favorites") as? [Int] {
            array.append(pageIndex)
            saveAndSynchornize(array)
        } else {
            let array = [pageIndex]
            saveAndSynchornize(array)
        }
    }
    
    private func unfavorite(pageIndex: Int) {
        guard var array = UserDefaults.standard.array(forKey: "favorites") as? [Int] else { fatalError("Nothing favorited to unfavorite") }
        guard let firstPageIndex = array.firstIndex(of: pageIndex) else { fatalError("Invalid page index") }
        array.remove(at: firstPageIndex)
        saveAndSynchornize(array)
    }
    
    private func saveAndSynchornize(_ array: [Int]) {
        UserDefaults.standard.set(array, forKey: "favorites")
        UserDefaults.standard.synchronize()
    }
}
