//
//  NoZoomOutPDFView.swift
//  Ketab
//
//  Created by Vortex on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import PDFKit

final class NoZoomOutPDFView: PDFView {
    
    init() {
        super.init(frame: .zero)
        NotificationCenter.default.addObserver(self, selector: #selector(update), name: .PDFViewDocumentChanged, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func update() {
        DispatchQueue.main.async {
            self.autoScales = true
            self.maxScaleFactor = 4.0
            self.minScaleFactor = self.scaleFactorForSizeToFit
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    override func addGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer is UILongPressGestureRecognizer {
            gestureRecognizer.isEnabled = false
        }
        
        super.addGestureRecognizer(gestureRecognizer)
    }
}
