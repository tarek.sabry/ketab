//
//  BoutrosFont.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

enum BoutrosFont {
    case regular
    case medium
    
    func getFont(ofSize size: CGFloat) -> UIFont {
        switch self {
        case .regular:
            return UIFont(name: "BoutrosAsma", size: size) ?? .systemFont(ofSize: size)
        case .medium:
            return UIFont(name: "BoutrosART-Medium", size: size) ?? .systemFont(ofSize: size)
        }
    }
}
