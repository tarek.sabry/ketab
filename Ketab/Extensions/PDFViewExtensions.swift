//
//  PDFViewExtensions.swift
//  Ketab
//
//  Created by Vortex on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import PDFKit

extension PDFView {
    func getCurrentPageAsImage() -> UIImage {
        guard let page = currentPage else { return UIImage() }
        let rect = page.bounds(for: .mediaBox).applying(page.transform(for: .mediaBox))
        let renderer = UIGraphicsImageRenderer(size: rect.size)
        return renderer.image { context in
            let cgContext = context.cgContext
            let rect = cgContext.boundingBoxOfClipPath
            cgContext.setFillColor(gray: 1, alpha: 1)
            cgContext.fill(rect)
            cgContext.translateBy(x: 0, y: rect.size.height)
            cgContext.scaleBy(x: 1, y: -1)
            page.draw(with: .mediaBox, to: cgContext)
        }
    }
    
    func getCurrentPageIndex() -> Int {
        guard
            let currentPage = currentPage,
            let pageLabel = currentPage.label,
            var pageIndex = Int(pageLabel)
        else { fatalError("Unable to get current page index") }
        pageIndex = pageIndex - 1
        return pageIndex
    }
}
