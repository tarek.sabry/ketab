//
//  UIColorExtensions.swift
//  Ketab
//
//  Created by Vortex on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var sideMenuGray: UIColor { return UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1) }
    
    static var cellBorderColor: UIColor { return UIColor(red: 240/255, green: 245/255, blue: 237/255, alpha: 1) }
    
    static var pageNumberColor: UIColor { return  #colorLiteral(red: 0.8410733938, green: 0.6225314736, blue: 0.2181198001, alpha: 1) }
    
    public convenience init(_ rgbValue : UInt32, alpha: CGFloat = 1.0) {
        
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8) / 256.0
        let blue = CGFloat(rgbValue & 0xFF) / 256.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
}
