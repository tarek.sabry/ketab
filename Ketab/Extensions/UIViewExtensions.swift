//
//  UIViewExtensions.swift
//  Ketab
//
//  Created by Vortex on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

extension UIView {
    
    func fadeIn(duration: TimeInterval = 0.2, completionHandler: @escaping () -> ()) {
        UIView.animate(withDuration: duration, delay: 0.0, options: [.transitionCrossDissolve], animations: {
            self.alpha = 1.0
        }, completion: { _ in
            completionHandler()
        })
    }
    
    func fadeOut(duration: TimeInterval = 0.2, completionHandler: @escaping () -> ()) {
        UIView.animate(withDuration: duration, delay: 0.0, options: [.transitionCrossDissolve], animations: {
            self.alpha = 0.0
        }, completion: { _ in
            completionHandler()
        })
    }
}
