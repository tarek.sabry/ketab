//
//  NSObjectExtensions.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import Foundation

extension NSObject {
    class var className: String {
        return "\(self)"
    }
}
