//
//  FavoriteView.swift
//  Ketab
//
//  Created by Vortex on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class FavoritePopupView: UIView {
    
    private lazy var containerView: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var favoriteImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var favoriteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = BoutrosFont.regular.getFont(ofSize: 17)
        return label
    }()
    
    private let favorited: Bool
    
    init(favorited: Bool, frame: CGRect) {
        self.favorited = favorited
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.45)
        alpha = 0.0
        layoutUI()
        if favorited {
            favoriteImageView.image = UIImage(named: "addfavpop")
            favoriteLabel.text = "تم إضافة الصفحة للمفضلة"
        } else {
            favoriteImageView.image = UIImage(named: "removefavpop")
            favoriteLabel.text = "تم حذف الصفحة من المفضلة"
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(favoriteImageView)
        containerView.addSubview(favoriteLabel)
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/1.17),
        ])
    }
    
    private func setupFavoriteImageViewConstraints() {
        NSLayoutConstraint.activate([
            favoriteImageView.heightAnchor.constraint(equalToConstant: 80),
            favoriteImageView.widthAnchor.constraint(lessThanOrEqualToConstant: 80),
            favoriteImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 24),
            favoriteImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
    
    private func setupFavoriteLabelConstraints() {
        NSLayoutConstraint.activate([
            favoriteLabel.centerXAnchor.constraint(equalTo: favoriteImageView.centerXAnchor),
            favoriteLabel.topAnchor.constraint(equalTo: favoriteImageView.bottomAnchor, constant: 24),
            favoriteLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -24)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupFavoriteImageViewConstraints()
        setupFavoriteLabelConstraints()
    }
    
}
