
//
//  HomeViewController.swift
//  Ketab
//
//  Created by Vortex on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import PDFKit
import SideMenu

class HomeViewController: UIViewController, PDFNavigationDelegate {

    private let mainView = HomeView()
    
    private lazy var swipeUpGestureRecognizer: UISwipeGestureRecognizer = {
        let gestureRecognizer = UISwipeGestureRecognizer()
        gestureRecognizer.direction = .up
        return gestureRecognizer
    }()
    
    private lazy var swipeDownGestureRecognizer: UISwipeGestureRecognizer = {
        let gestureRecognizer = UISwipeGestureRecognizer()
        gestureRecognizer.direction = .down
        return gestureRecognizer
    }()
    
    private var favoriteView: FavoritePopupView?
    
    private let favoritesManager: FavoritesManagerProtocol
    
    init(favoritesManager: FavoritesManagerProtocol) {
        self.favoritesManager = favoritesManager
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "الرئيسية"
        loadPDF()
        setupNavigationBarButtons()
        initSideMenu()
        setupSideMenuGesture()
        setupGestureRecognizers()
        NotificationCenter.default.addObserver(self, selector: #selector(scrollPdfToTop), name: NSNotification.Name(rawValue: "statusBarTapped"), object: nil)
    }
    
    private func loadPDF() {
        guard let pdfFile = Bundle.main.url(forResource: "book", withExtension: "pdf") else {
            return
        }
        mainView.load(pdfDocument: PDFDocument(url: pdfFile))
    }
    
    private func setupNavigationBarButtons() {
        let menuButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .height(20)
            .image(UIImage(named: "menu"))
            .width(20)
            .action(target: self, selector: #selector(presentSideMenu))
            .build()
        
        let shareButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .height(30)
            .image(UIImage(named: "share"))
            .width(30)
            .action(target: self, selector: #selector(sharePage))
            .build()
        
        let emptySpace = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .height(20)
            .width(5)
            .build()
        
        let likeButton = NavigationBarButtonBuilderFactory
            .buttonWithImage()
            .height(30)
            .image(UIImage(named: "fav"))
            .width(30)
            .action(target: self, selector: #selector(toggleFavorite))
            .build()
        
        navigationItem.leftBarButtonItem = menuButton
        navigationItem.rightBarButtonItems = [shareButton, emptySpace, likeButton]
    }
    
    private func setupSideMenuGesture() {
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: view, forMenu: .right)
    }
    
    @objc private func presentSideMenu() {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    @objc private func sharePage() {
        let currentPageImage = mainView.getCurrentPageAsImage()
        let shareActivityViewController = UIActivityViewController(activityItems: [currentPageImage], applicationActivities: nil)
        present(shareActivityViewController, animated: true, completion: nil)
    }
    
    @objc private func scrollPdfToTop() {
        if !(presentedViewController is UISideMenuNavigationController) {
            mainView.scrollPdfToTop()
        }
    }
    
    private func setupGestureRecognizers() {
        mainView.addGestureRecognizer(swipeUpGestureRecognizer)
        swipeUpGestureRecognizer.addTarget(mainView, action: #selector(mainView.scrollToNextPage))
        
        mainView.addGestureRecognizer(swipeDownGestureRecognizer)
        swipeDownGestureRecognizer.addTarget(mainView, action: #selector(mainView.scrollToPreviousPage))
    }
    
    @objc private func toggleFavorite() {
        let favorited = favoritesManager.toggleFavorite(at: mainView.getCurrentPageIndex())
        showFavoriteView(for: 1, favorited: favorited)
    }
    
    func showFavoriteView(for duration: Double, favorited: Bool) {
        favoriteView = FavoritePopupView(favorited: favorited, frame: navigationController!.view.frame)
        navigationController?.view.addSubview(favoriteView!)
        favoriteView?.fadeIn {
            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                self.favoriteView?.fadeOut {
                    self.favoriteView?.removeFromSuperview()
                }
            }
        }
    }
    
    private func initSideMenu() {
        SideMenuManager.default.menuRightNavigationController = UISideMenuNavigationController(rootViewController: SideMenuViewController(delegate: self))
    }
    
    func navigate(to index: Int) {
        mainView.goTo(index: index)
    }
}
