//
//  HomeView.swift
//  Ketab
//
//  Created by Vortex on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import PDFKit

import Foundation
import PDFKit

class HomeView: UIView {
    
     private lazy var pdfView: NoZoomOutPDFView = {
        let pdfView = NoZoomOutPDFView()
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        pdfView.autoScales = true
        pdfView.scaleFactor = 2
        pdfView.displayMode = .singlePage
        pdfView.minScaleFactor = 2
        pdfView.maxScaleFactor = 6
        pdfView.backgroundColor = .white
        return pdfView
    }()
    
    private lazy var zoomOutButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "zoom-")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(zoomOut), for: .touchUpInside)
        return button
    }()
    
    private lazy var zoomInButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "zoom+")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(zoomIn), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(pdfView)
        addSubview(zoomOutButton)
        addSubview(zoomInButton)
    }
    
    private func setupPdfViewConstraints() {
        NSLayoutConstraint.activate([
            pdfView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            pdfView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            pdfView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            pdfView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func setupZoomOutButtonConstraints() {
        NSLayoutConstraint.activate([
            zoomOutButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -4),
            zoomOutButton.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 4),
            zoomOutButton.heightAnchor.constraint(equalToConstant: 45),
            zoomOutButton.widthAnchor.constraint(equalToConstant: 45)
        ])
    }
    
    private func setupZoomInButtonConstraints() {
        NSLayoutConstraint.activate([
            zoomInButton.bottomAnchor.constraint(equalTo: zoomOutButton.topAnchor, constant: -4),
            zoomInButton.leadingAnchor.constraint(equalTo: zoomOutButton.leadingAnchor, constant: 0),
            zoomInButton.heightAnchor.constraint(equalToConstant: 45),
            zoomInButton.widthAnchor.constraint(equalToConstant: 45)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupPdfViewConstraints()
        setupZoomOutButtonConstraints()
        setupZoomInButtonConstraints()
    }
    
    func load(pdfDocument: PDFDocument?) {
        pdfView.document = pdfDocument
    }
    
    @objc func scrollPdfToTop() {
        pdfView.goToFirstPage(nil)
    }
    
    @objc func scrollToNextPage() {
        pdfView.goToNextPage(nil)
    }
    
    @objc func scrollToPreviousPage() {
        pdfView.goToPreviousPage(nil)
    }
    
    @objc func zoomIn() {
        pdfView.zoomIn(nil)
    }
    
    @objc func zoomOut() {
        pdfView.zoomOut(nil)
    }
    
    func getCurrentPageAsImage() -> UIImage {
        return pdfView.getCurrentPageAsImage()
    }
    
    func getCurrentPageIndex() -> Int {
        return pdfView.getCurrentPageIndex()
    }
    
    func goTo(index: Int) {
        guard let pageIndex = pdfView.document?.page(at: index) else { return }
        pdfView.go(to: pageIndex)
    }
}
