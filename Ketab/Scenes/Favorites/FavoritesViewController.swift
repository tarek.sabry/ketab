//
//  FavoriteViewController.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {
    
    private let mainView = FavoritesView()
    
    private let favoritesManager: FavoritesManagerProtocol
    private weak var navigationDelegate: PDFNavigationDelegate?
    private var favorites: [Int]
    
    init(favoritesManager: FavoritesManagerProtocol, delegate: PDFNavigationDelegate?) {
        self.favoritesManager = favoritesManager
        self.favorites = favoritesManager.savedIndicies
        self.navigationDelegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        navigationItem.title = "المفضلة"
        UINavigationBar.appearance().titleTextAttributes = [
            .foregroundColor: UIColor.white,
            .font: BoutrosFont.medium.getFont(ofSize: 17)
        ]
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
}

extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favorites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FavoriteTableViewCell.className) as! FavoriteTableViewCell
        cell.configure(pageNumber: favorites[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            favoritesManager.toggleFavorite(at: favorites[indexPath.row])
            favorites.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = favorites[indexPath.row]
        navigationController?.popViewController(animated: true)
        navigationDelegate?.navigate(to: item)
    }
}
