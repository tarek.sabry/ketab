//
//  FavoriteTableViewCell.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {
    
    private lazy var containerView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "favoritecellBG")
        return imageView
    }()
   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        textLabel?.font = BoutrosFont.regular.getFont(ofSize: 16)
        textLabel?.textAlignment = .center
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func configure(pageNumber: Int) {
        textLabel?.text =  "رقم الصفحة " + String(pageNumber + 1)
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(backgroundImageView)
        sendSubviewToBack(containerView)
        sendSubviewToBack(backgroundImageView)
    }
    
    
    func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupBackgroudImageViewConstraints()
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 8),
            containerView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16),
            containerView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16),
            containerView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupBackgroudImageViewConstraints() {
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            backgroundImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            backgroundImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            backgroundImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        ])
    }

}
