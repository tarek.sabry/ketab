//
//  BookContentsTableViewCell.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class BookContentsTableViewCell: UITableViewCell {
    
    private lazy var containerView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "favoritecellBG")
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = .byClipping
        label.numberOfLines = 0
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.font = BoutrosFont.regular.getFont(ofSize: 15)
        return label
    }()
    
    
    private lazy var pageNumberLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.layer.masksToBounds = true
        label.font = .systemFont(ofSize: 15)
        label.layer.cornerRadius = 17.5
        label.sizeToFit()
        label.backgroundColor = UIColor.pageNumberColor
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        textLabel?.textAlignment = .center
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func configure(pageNumber: Int, title: String ) {
        titleLabel.text =  title
        pageNumberLabel.text = "\(pageNumber + 1)"
    }
    
    private func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(backgroundImageView)
        containerView.addSubview(pageNumberLabel)
        containerView.addSubview(titleLabel)
        sendSubviewToBack(containerView)
        sendSubviewToBack(backgroundImageView)
    }
    
    
    func layoutUI() {
        addSubviews()
        setupContainerViewConstraints()
        setupBackgroudImageViewConstraints()
        setupPageNumberLabelConstraints()
        setupTitleLabelConstraints()
    }
    
    private func setupContainerViewConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 0),
            containerView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16),
            containerView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16),
            containerView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -8)
        ])
    }
    
    private func setupBackgroudImageViewConstraints() {
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            backgroundImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            backgroundImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            backgroundImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        ])
    }
    
    private func setupPageNumberLabelConstraints() {
        NSLayoutConstraint.activate([
            pageNumberLabel.topAnchor.constraint(equalTo: backgroundImageView.topAnchor, constant: 15),
            pageNumberLabel.leadingAnchor.constraint(equalTo: backgroundImageView.leadingAnchor, constant: 10),
            pageNumberLabel.heightAnchor.constraint(equalToConstant: 35),
            pageNumberLabel.widthAnchor.constraint(equalToConstant: 35)
        ])
    }
    
    private func setupTitleLabelConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: backgroundImageView.topAnchor, constant: 18),
            titleLabel.leadingAnchor.constraint(equalTo: pageNumberLabel.trailingAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: backgroundImageView.trailingAnchor, constant: -25),
            titleLabel.heightAnchor.constraint(equalToConstant: 30),
            titleLabel.widthAnchor.constraint(equalToConstant: 85)
       ])
    }

}
