//
//  BookContentsViewController.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class BookContentsViewController: UIViewController {
    
    private let mainView = BookContentsView()
    
    let bookContents = [
        BookContent(title:"البداية", pageNumber: 0),
        BookContent(title:"اية و حديث", pageNumber: 3),
        BookContent(title:"اهداء لكل مسلم", pageNumber: 4),
        BookContent(title:"المقدمة", pageNumber: 5),
        BookContent(title:"معنى الصلاة على النبي", pageNumber: 7),
        BookContent(title:"مدخل الموضوع", pageNumber: 8),
        BookContent(title:"مالفرق بين هذه الصيغ؟و ما فائدته؟", pageNumber: 9),
        BookContent(title:"أنفع الدعاء", pageNumber: 11),
        BookContent(title:"الصيغة الأولى", pageNumber: 13),
        BookContent(title:"الصيغة الثانية", pageNumber: 16),
        BookContent(title:"الصيغة الثالثة", pageNumber: 21),
        BookContent(title:"الصيغة الرابعة", pageNumber: 22),
        BookContent(title:"الصيغة الخامسة", pageNumber: 24),
        BookContent(title:"الصيغة السادسة", pageNumber: 25),
        BookContent(title:"تذكر في كل جمعة", pageNumber: 37),
        BookContent(title:"الخاتمة", pageNumber: 38),
        BookContent(title:"قائمة المصادر", pageNumber: 39),
    ]
    
    private weak var delegate: PDFNavigationDelegate?
    
    init(delegate: PDFNavigationDelegate?) {
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        navigationItem.title = "محتويات الكتاب"
        UINavigationBar.appearance().titleTextAttributes = [
            .foregroundColor: UIColor.white,
            .font: BoutrosFont.medium.getFont(ofSize: 17)
        ]
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
        
    }
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate = self
    }
    
    
}


extension BookContentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookContents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookContentsTableViewCell.className) as! BookContentsTableViewCell
        let bookContent = bookContents[indexPath.row]
        cell.configure(pageNumber: bookContent.pageNumber, title: bookContent.title)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let page = bookContents[indexPath.row].pageNumber
        navigationController?.popViewController(animated: true)
        delegate?.navigate(to: page)
    }
}
