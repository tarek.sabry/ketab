//
//  BookContents.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/4/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

struct BookContent {
   var title: String
   var pageNumber: Int
}
