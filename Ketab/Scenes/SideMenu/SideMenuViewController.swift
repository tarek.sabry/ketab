//
//  SideMenuViewController.swift
//  Ketab
//
//  Created by Vortex on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

protocol PDFNavigationDelegate: class {
    func navigate(to index: Int)
}

class SideMenuViewController: UIViewController {
    
    
    private let mainView = SideMenuView()
    let sideMenuItems = [
        SideMenuItem(image: "home", name: "الرئيسية"),
        SideMenuItem(image: "fehres", name: "محتويات الكتاب"),
        SideMenuItem(image: "favourite", name: "المفضلة"),
        SideMenuItem(image: "shareapp", name: "شارك التطبيق")
    ]
    
    private weak var delegate: PDFNavigationDelegate?
    
    init(delegate: PDFNavigationDelegate) {
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupTableView()
    }
    
    private func setupTableView() {
        mainView.tableView.dataSource = self
        mainView.tableView.delegate  = self
    }
}

extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableViewCell.className) as! SideMenuTableViewCell
        let item = sideMenuItems[indexPath.row]
        cell.configure(name: item.name, image: item.image, isOdd: indexPath.row % 2  == 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
            delegate?.navigate(to: 0)
        } else if indexPath.row == 1 {
            navigationController?.pushViewController(BookContentsViewController(delegate: delegate), animated: true)
        } else if indexPath.row == 2 {
            navigationController?.pushViewController(FavoritesViewController(favoritesManager: FavoritesManager(), delegate: delegate), animated: true)
        } else if indexPath.row == 3 {
            let url = URL(string: "https://itunes.apple.com/us/app/idxxxxxxx")!
            let text = "حمل كتاب يا رسول الله كيف نصلي عليك الان"
            let activityController = UIActivityViewController(activityItems: [url, text], applicationActivities: nil)
            present(activityController, animated: true, completion: nil)
        }
        
    }
}
