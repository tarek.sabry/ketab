//
//  sideMenuTableViewCell.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        textLabel?.font = BoutrosFont.regular.getFont(ofSize: 16)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func configure(name: String, image: String, isOdd: Bool) {
        textLabel?.text = name
        imageView?.image = UIImage(named: image)
        if isOdd {
            contentView.backgroundColor = .sideMenuGray
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.backgroundColor = .white
    }
}
