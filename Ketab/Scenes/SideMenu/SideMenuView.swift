//
//  SideMenuView.swift
//  Ketab
//
//  Created by Vortex on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

class SideMenuView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 0.4029678106, green: 0.5993874669, blue: 0.2768114209, alpha: 1)
        return view
    }()
    
    private lazy var headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "sideheader")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(SideMenuTableViewCell.self, forCellReuseIdentifier: SideMenuTableViewCell.className)
        tableView.separatorStyle = .none
        return tableView
    }()
    
    private func addSubviews() {
        addSubview(headerView)
        addSubview(headerImageView)
        addSubview(tableView)
    }
    
    private func setupHeaderViewConstraints() {
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            headerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupHeaderImageViewConstraints() {
        let headerImageViewTopAnchor = (UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0) > 24 ? headerImageView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 5) : headerImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0)
        NSLayoutConstraint.activate([
            headerImageViewTopAnchor,
            headerImageView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: -1),
            headerImageView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: 0),
        ])
    }
    
    private func setupTableViewConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: headerImageView.bottomAnchor, constant: 25),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
    }
    
    private func layoutUI() {
        addSubviews()
        setupHeaderViewConstraints()
        setupHeaderImageViewConstraints()
        setupTableViewConstraints()
    }
}
