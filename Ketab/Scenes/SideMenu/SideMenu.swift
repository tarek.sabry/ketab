//
//  SideMenu.swift
//  Ketab
//
//  Created by iOS ibtdi.com on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit

struct SideMenuItem {
    let image: String
    let name: String
}

