//
//  AppDelegate.swift
//  Ketab
//
//  Created by Vortex on 3/3/19.
//  Copyright © 2019 Ibtdi. All rights reserved.
//

import UIKit
import SideMenu

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.makeKeyAndVisible()
        UINavigationBar.appearance().barTintColor = UIColor(0x679947)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [
            .foregroundColor: UIColor.white,
            .font: BoutrosFont.medium.getFont(ofSize: 17)
        ]
        initSideMenu()
        window?.rootViewController = UINavigationController(rootViewController: HomeViewController(favoritesManager: FavoritesManager()))
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        let statusBarRect = UIApplication.shared.statusBarFrame
        guard let touchPoint = event?.allTouches?.first?.location(in: self.window) else { return }
        
        if statusBarRect.contains(touchPoint) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "statusBarTapped"), object: nil)
        }
    }
    
    private func initSideMenu() {
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAnimationFadeStrength = 0.5
        SideMenuManager.default.menuWidth = UIWindow().bounds.width / 1.3
    }
}

